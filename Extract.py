import scipy.io as sio
import math
import numpy as np
import cv2
import tensorflow as tf
from scipy import misc
import sys
import os
import argparse
import align.detect_face
import cv2

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
os.environ['CUDA_VISIBLE_DEVICES'] = '5'


BASE_FOLDER = 'D:/LeTranBaoCuong/GenderDataProcess/wiki_crop/'
SAVE_FOLDER = 'D:/LeTranBaoCuong/GenderDataProcess/gender_data/'

def detectFace(tmp_img):
    minsize = 20  # minimum size of face
    threshold = [0.6, 0.7, 0.7]  # three steps's threshold
    factor = 0.709  # scale factor
    gpu_memory_fraction = 1.0
    margin = 0
    image_size = 160
    # load model
    with tf.Graph().as_default():
        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=gpu_memory_fraction)
        sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False))
        with sess.as_default():
            pnet, rnet, onet = align.detect_face.create_mtcnn(sess, None)

    result = []
    for i in range(len(tmp_img)):
        print('Detect face... ', str(i + 1), '/', str(len(tmp_img)), end='\r')
        img = tmp_img[i][0]
        label = tmp_img[i][1]
        img_size = np.asarray(img.shape)[0:2]

        bounding_boxes, points = align.detect_face.detect_face(img, minsize, pnet, rnet, onet, threshold, factor)

        nrof_face = len(bounding_boxes)

        if (nrof_face >= 1):
            for j in range(1):
                det = np.squeeze(bounding_boxes[j, 0:4])
                bb = np.zeros(4, dtype=np.int32)
                bb[0] = np.maximum(det[0] - margin / 2, 0)
                bb[1] = np.maximum(det[1] - margin / 2, 0)
                bb[2] = np.minimum(det[2] + margin / 2, img_size[1])
                bb[3] = np.minimum(det[3] + margin / 2, img_size[0])
                cropped = img[bb[1]:bb[3], bb[0]:bb[2], :]
                result.append((cropped,label))
    return result



if __name__ == '__main__':
    mat = sio.loadmat('D:/LeTranBaoCuong/MultiTask/wiki_crop/wiki.mat')

    columm = ['dob', 'year', 'path', 'gender', 'name', 'face', 'face_score', 'second_face_score']

    wiki = mat['wiki'][0][0]

    number_person = wiki[0].shape[1]

    data = []
    tmp_img = []
    number_img = 0


    for person in range(number_person):
        print('Processing person ',str(person+1),'/',str(number_person),end = '\r')
        filePath = BASE_FOLDER + wiki[2][0][person][0]

        gender = wiki[3][0][person]
        face_score = wiki[6][0][person]
        second_face_score = wiki[7][0][person]

        if (face_score > 0) and (np.isnan(second_face_score)) and (np.isnan(gender) == False):
            img = cv2.imread(filePath)
            tmp_img.append((img,gender))

    data = detectFace(tmp_img)

    print('Number of images: ', str(len(data)), '/', str(number_person))
    np.save(SAVE_FOLDER + 'data.npy',data)